const ManageCards = artifacts.require("ManageCards");
const Deck = artifacts.require("Deck");
const Market = artifacts.require("Market");
const ERC20 = artifacts.require("ERC20");
const ERC1155PresetMinterPauser = artifacts.require("ERC1155PresetMinterPauser");
const expect = require('chai').expect;
const utils = require("./helpers/utils");

contract("Test", (accounts) => {
    let [owner, alice, bob] = accounts;
    let erc1155;

    beforeEach(async () => {
        erc1155 = await ERC1155PresetMinterPauser.new({from: owner});
    });
    context("selling tokens", async () => {
        it("should be able to create a new card", async () => {
            const res = await erc1155.newCard([alice], [10], { name: 'bnb', element: 'fire', mintable: true }, {from: owner});
            expect(res.receipt.status).to.equal(true);
        });
    });


});


contract("Market", (accounts) => {
    let [owner, alice, bob] = accounts;
    let erc1155;
    let erc20;
    let market;

    const amounts = [10, 10, 10, 10];
    const fee = 5;

    const salePrice = 13;
    const claimableForSeller = Number((salePrice * (1 - fee/100)).toString().split('.')[0]);
    const claimableForOwner = salePrice - claimableForSeller;

    beforeEach(async () => {
        erc1155 = await ERC1155.new("testUri", {from: owner});
        erc20 = await ERC20.new("TEST", "TST", {from: owner});
        market = await Market.new(fee, erc20.address, erc1155.address, {from: owner});

        await erc1155.newCard([alice], [10], 0, {from: owner});

        await erc20.approve(market.address, 5000, {from: bob});
        await erc20.approve(market.address, 5000, {from: owner});
        await erc20.approve(market.address, 5000, {from: alice});
        await erc1155.setApprovalForAll(market.address, true, {from: alice});
        await erc1155.setApprovalForAll(market.address, true, {from: bob});
        await erc1155.setApprovalForAll(market.address, true, {from: owner});
    });

    context("selling tokens", async () => {

        context("if own the token", async () => {
            let sellingRes;
            beforeEach(async () => {
                sellingRes = await market.sellToken(1, 500, {from: alice});
            });

            it ("should not throw an error", async () => {
                expect(sellingRes.receipt.status).to.equal(true);
            });
            it ("should have an incremented ERC1155 contract balance", async () => {
                const balanceOfContract = (await erc1155.balanceOf(market.address, 1)).words[0];
                expect(balanceOfContract).to.equal(1);
            });


        });
        context("if don't own the token", async () => {
            let sellingError;
            beforeEach(async () => {
                try {
                    await market.sellToken(1, 500, {from: bob});
                }
                catch (e) {
                    sellingError = e;
                }
            });

            it ("should throw an error", async () => {
                expect(typeof sellingError).not.to.equal('undefined');
            });
        });
    });

    context("buying tokens", async () => {
        beforeEach(async () => {
            await market.sellToken(1, salePrice, {from: alice});
        });

        context("with enough money", async () => {
            let buyerBalance = 5000;
            let buyingRes;
            beforeEach(async () => {
                await erc20.transfer(bob, buyerBalance, {from: owner});
                buyingRes = await market.buyToken(1, {from: bob});
            });

            it("should not throw an error", async () => {
                expect(buyingRes.receipt.status).to.equal(true);
            });
            it ("should have an incremented ERC1155 balance for the buyer", async () => {
                const balanceOfBuyer = (await erc1155.balanceOf(bob, 1, {from: bob})).words[0];
                expect(balanceOfBuyer).to.equal(1);
            });
            it ("should have a decremented ERC20 balance for the buyer", async () => {
                const balanceOfBuyer = (await erc20.balanceOf(bob, {from: bob})).words[0];
                expect(balanceOfBuyer).to.equal(buyerBalance - salePrice);
            });
            it ("should have a decremented ERC1155 balance for the seller", async () => {
                const balanceOfSeller = (await erc1155.balanceOf(alice, 1, {from: alice})).words[0];
                expect(balanceOfSeller).to.equal(amounts[0] - 1);
            });
            it ("should have an unchanged ERC20 balance for the seller", async () => {
                const balanceOfSeller = (await erc20.balanceOf(alice, {from: alice})).words[0];
                expect(balanceOfSeller).to.equal(0);
            });
            it("should have the right claimable tokens for the seller", async () => {
                const claimable = (await market.claimableTokens(alice)).words[0];
                expect(claimable).to.equal(claimableForSeller);
            });
            it("should have the right claimable tokens for the owner", async () => {
                const claimable = (await market.claimableTokens(owner)).words[0];
                expect(claimable).to.equal(claimableForOwner);
            });

            context("claiming ERC20 token", async () => {
                let ownerClaimRes;
                let sellerClaimRes;
                let ownerBalance;
                beforeEach(async () => {
                    ownerClaimRes = await market.claimTokens({from: owner});
                    sellerClaimRes = await market.claimTokens({from: alice});
                    ownerBalance = (await erc20.balanceOf(owner)).words[0];
                })
                it ("should not throw an error for the seller", async () => {
                   expect(sellerClaimRes.receipt.status).to.equal(true);
                });
                it ("should not throw an error for the owner", async () => {
                    expect(ownerClaimRes.receipt.status).to.equal(true);
                });
                it ("should have the correct new balance for the seller", async () => {
                    const balance = (await erc20.balanceOf(alice)).words[0];
                    expect(balance).to.equal(claimableForSeller);
                });
                it ("should have the correct new balance for the owner", async () => {
                    const balance = (await erc20.balanceOf(owner)).words[0];
                    expect(balance).to.equal(claimableForOwner + ownerBalance);
                });
            });

            context ('buying a sold item', async () => {
                let buyingError;
                let erc20OwnerBalance;

                beforeEach(async () => {

                    erc20OwnerBalance = (await erc20.balanceOf(owner)).words[0];

                    try {
                        await market.buyToken(1, {from: owner});
                    }
                    catch (e) {
                        buyingError = e;
                    }
                });

                it ("should throw an error", async () => {
                    expect(typeof buyingError).not.to.equal('undefined');
                });
                it ("should have an unchanged ERC20 buyer balance", async () => {
                    const erc20Balance = (await erc20.balanceOf(owner)).words[0];
                    expect(erc20Balance).to.equal(erc20OwnerBalance);
                });
            });
        });

        context("without enough money", async () => {
            let buyingError;

            beforeEach(async () => {
                try {
                    await market.buyToken(1, {from: bob});
                }
                catch (e) {
                    buyingError = e;
                }
            })

            it ("should throw an error", async () => {
                expect(typeof buyingError).not.to.equal('undefined');
            });
            it ("should have an unchanged ERC1155 buyer balance", async () => {
                const erc20Balance = (await erc1155.balanceOf(bob, 1)).words[0];
                expect(erc20Balance).to.equal(0);
            });
            it ("should have an unchanged ERC1155 contract balance", async () => {
                const erc20Balance = (await erc1155.balanceOf(market.address, 1)).words[0];
                expect(erc20Balance).to.equal(1);
            });
        });
    });


});


contract("Deck", (accounts) => {
    let [owner, alice, bob] = accounts;
    let erc1155;
    let erc20;
    let deckContract;
    const amounts = [10, 10, 10, 10];

    beforeEach(async () => {
        erc1155 = await ERC1155.new("testUri", {from: owner});
        erc20 = await ERC20.new("TEST", "TST", {from: owner});
        deckContract = await Deck.new("Supreme Deck", 40, 5, erc20.address, erc1155.address, {from: owner});

        for (let i = 0 ; i < amounts.length ; i++) {
            await erc1155.newCard([deckContract.address], [amounts[i]], 0, {from: owner});
        }

        await erc20.transfer(alice, 5000, {from: owner});
        await erc20.transfer(bob, 5000, {from: owner});
    });

    context("Opening deck", async () => {
        it ('should be able to open deck if enought money', async () => {
            await deckContract.activateDeck({from: owner});
            await erc20.approve(deckContract.address, 40, {from: alice});

            const res = await deckContract.openDeck({from: alice});
            expect(res.receipt.status === true, "Status is not true");

            const balance = await erc1155.totalBalanceOf(alice, {from: alice});
            expect(balance === 5, "The total balance of Alice is not equal to 5");
        });
        it('should not be able to open deck if not activated', async () => {
            await erc20.approve(deckContract.address, 40, {from: alice});
            await utils.shouldThrow(deckContract.openDeck({from: alice}));
        });
        it('should not be able to open deck if not enought money', async () => {
            await deckContract.activateDeck({from: owner});

            await erc20.approve(deckContract.address, 40, {from: alice});
            await erc20.transfer(bob, 4961, {from: alice});

            await utils.shouldThrow(deckContract.openDeck({from: alice}));
        });
        it('should desactive the contract if no card anymore', async () => {
            await deckContract.activateDeck({from: owner});

            await erc20.approve(deckContract.address, 40 * 9, {from: alice});

            for (let i = 0 ; i < 8 ; i++) {
                await deckContract.openDeck({from: alice});
            }

            await utils.shouldThrow(deckContract.openDeck({from: alice}));
        });
    });

    context("activate deck", async () => {
        it ('should be able active if owner and correct amount of cards', async () => {
            const res = await deckContract.activateDeck({from: owner});
            expect(res.receipt.status).to.equal(true);
        });
        it ('should not be able active if not owner and correct amount of cards', async () => {
            await utils.shouldThrow(deckContract.activateDeck({from: alice}));
        });
        it ('should not be able active if owner but not correct amount of cards', async () => {
            await erc1155.newCard([deckContract.address], [1], 0, {from: owner});
            await utils.shouldThrow(deckContract.activateDeck({from: owner}));
        });
        it ('should not be able active if not owner and not correct amount of cards', async () => {
            await erc1155.newCard([deckContract.address], [1], 0, {from: owner});
            await utils.shouldThrow(deckContract.activateDeck({from: alice}));
        });
    });

});

contract("manageCards", (accounts) => {

    let [owner, alice, bob] = accounts;
    let manageCards;
    let erc1155;

    beforeEach(async () => {
        erc1155 = await ERC1155.new("testUri", {from: owner, unsafeAllowCustomTypes: true});
        manageCards = await ManageCards.new(erc1155.address, {from: owner});

        await erc1155.grantRole(await erc1155.MINTER_ROLE(), manageCards.address, {from: owner});
    });

    context("with setting mergeable cards", async () => {
        it("should be able to set mergeable cards with owner", async () => {

            // Set cards that can be merged
            const result = await manageCards.setMergeableCard(1, [2,3], {from: owner});
            expect(result.receipt.status).to.equal(true);

            const cards = await manageCards.getMergeableCards({from: owner});
            expect(cards[0][0].words[0]).to.equal(1);
            expect(cards[1][0][0].words[0]).to.equal(2);
            expect(cards[1][0][1].words[0]).to.equal(3);
        });
        it("should not be able to set mergeable cards without owner", async () => {
            await utils.shouldThrow(manageCards.setMergeableCard(1, [2,3], {from: alice}));
        });
    });

    context("with minting cards", async () => {
        it("should be able to mint cards with owner", async () => {

            let occurrences = 4;

            for (let i = 1 ; i <= occurrences ; i++) {
                const aliceAmount = i;
                const bobAmount = i * 2;

                const result = await erc1155.newCard([alice, bob], [aliceAmount, bobAmount], 0, {from: owner});
                expect(result.logs[0].args.id.toNumber()).to.equal(i);

                const balanceOfAlice = (await erc1155.balanceOf(alice, i, {from: alice})).words[0];
                expect(balanceOfAlice).to.equal(aliceAmount);

                const balanceOfBob = (await erc1155.balanceOf(bob, i, {from: bob})).words[0];
                expect(balanceOfBob).to.equal(bobAmount);
            }
        });

        it("should not be able to mint cards without owner", async () => {
            await utils.shouldThrow(erc1155.newCard([alice, bob], [5, 5], 0, {from: alice}));
        });
    });

    context("with merging cards", async () => {
        it("should be able to merge cards", async () => {
            const id1 = (await erc1155.newCard([alice], [1], 0, {from: owner})).logs[0].args.id.toNumber();
            const id2 = (await erc1155.newCard([alice], [1], 0, {from: owner})).logs[0].args.id.toNumber();
            const id3 = (await erc1155.newCard([manageCards.address], [0], 0, {from: owner})).logs[0].args.id.toNumber();

            await manageCards.setMergeableCard(id3, [id1,id2], {from: owner});

            erc1155.setApprovalForAll(manageCards.address, true, {from: alice})

            const result = await manageCards.mergeCards(alice, id3, {from: alice});
            expect(result.receipt.status).to.equal(true);

            const balanceOfAlice = await erc1155.balanceOfBatch([alice, alice, alice], [id1, id2, id3], {from: alice});
            expect(balanceOfAlice[0].words[0]).to.equal(0);
            expect(balanceOfAlice[1].words[0]).to.equal(0);
            expect(balanceOfAlice[2].words[0]).to.equal(1);

            expect( (await erc1155.supplyOf(id1)) === 0, "The supply of the card 1 is not equal to 0");
            expect( (await erc1155.supplyOf(id2)) === 0, "The supply of the card 2 is not equal to 0");
            expect( (await erc1155.supplyOf(id3)) === 1, "The supply of the card 3 is not equal to 0");
        });

        it("should not be able to merge cards without expected parent cards", async () => {
            const id1 = (await erc1155.newCard([bob], [1], 0, {from: owner})).logs[0].args.id.toNumber();
            const id2 = (await erc1155.newCard([alice], [1], 0, {from: owner})).logs[0].args.id.toNumber();
            const id3 = (await erc1155.newCard([manageCards.address], [0], 0, {from: owner})).logs[0].args.id.toNumber();

            await manageCards.setMergeableCard(id3, [id1,id2], {from: owner});

            await utils.shouldThrow(manageCards.mergeCards(alice, id3, {from: alice}));
        });
    });

});


contract("Custom ERC1155", (accounts) => {
    let [owner, alice, bob] = accounts;
    let erc1155;
    const amounts = [1000, 20, 10000, 500, 2300, 10, 50000];

    beforeEach(async () => {
        erc1155 = await ERC1155.new("testUri", {from: owner});

        for (let i = 0 ; i < amounts.length ; i++) {
            await erc1155.newCard([alice, bob], [amounts[i]/2, amounts[i]/2], 0, {from: owner});
        }
    });

    context("with getting all balances of", async () => {
        it ('should be able to get all balances', async () => {
            const res = await erc1155.allBalancesOf(alice, {from: alice});

            expect(res.length === amounts.length, 'The answer have not the same length of amounts');

            for (let i = 0 ; i < amounts.length ; i++) {
               expect(res[i] === amounts[i] / 2, 'The amount of the id ' + i + 'is not correct');
            }
        });
        it ('should not be able to get all balances of a deck address', async () => {
            await erc1155.setDeckAddress(alice, {from: owner});
            await utils.shouldThrow(erc1155.allBalancesOf(alice, {from: bob}));
        });
        it ('should be able to get all balances of a deck address if owner', async () => {
            await erc1155.setDeckAddress(alice, {from: owner});

            const res = await erc1155.allBalancesOf(alice, {from: owner});

            expect(res.length === amounts.length, 'The answer have not the same length of amounts');

            for (let i = 0 ; i < amounts.length ; i++) {
                expect(res[i] === amounts[i] / 2, 'The amount of the id ' + i + 'is not correct');
            }
        });
    });

    context("with setting deck address", async () => {
        it ('should be able to set deck address with owner', async () => {
            const res = await erc1155.setDeckAddress(alice, {from: owner});
            expect(res.receipt.status).to.equal(true);

            await utils.shouldThrow(erc1155.totalBalanceOf(alice, {from: bob}));
        });
        it ('should not be able to set deck address without owner', async () => {
            await utils.shouldThrow(erc1155.setDeckAddress(alice, {from: bob}));
        });
    });
});
