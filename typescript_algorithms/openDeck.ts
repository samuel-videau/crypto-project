let deckCards = [100000000000,200000000000,400000000000,300000000000];
const cardsPerDeck = 5;
let aliceCards = [0,0,0,0]

function sum() {
    let sum = 0;
    deckCards.forEach(r => sum+=r);
    return sum;
}

function aliceSum() {
    let sum = 0;
    aliceCards.forEach(r => sum+=r);
    return sum;
}

function openDeck() {
    if (sum() == 0) {
        console.log("No cards in the deck");
        return;
    }

    let rands: number[] = [];
    let choosedCards: number[] = [];

    for (let i = 0 ; i < cardsPerDeck ; i++) {
        rands.push(Math.floor(Math.random() * (sum()-i)));
        choosedCards.push(0);
    }

    for (let i = 0 ; i < deckCards.length ; i++) {
        if(deckCards[i] != 0) {
            for (let j = 0 ; j < cardsPerDeck ; j++ ) {
                if (choosedCards[j] == 0 && deckCards[i] != 0) {
                    rands[j] -= deckCards[i];

                    if (rands[j] <= 0) {
                        choosedCards[j] = i + 1;
                        aliceCards[i]++;
                        deckCards[i]--;
                    }
                }
            }
        }
    }
}

for (let i = 0 ; i < 100000 ; i++) {
    openDeck();
}

let percentages: number[] = [];

aliceCards.forEach(card => {
    percentages.push((card/aliceSum()) * 100);
})
console.log(percentages);
