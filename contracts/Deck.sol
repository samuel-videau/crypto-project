pragma solidity ^0.8.0;

import "./token/ERC20/IERC20.sol";
import "./utils/math/SafeMath.sol";
import "./token/ERC1155/utils/ERC1155Holder.sol";
import "./token/ERC1155/IERC1155.sol";
import "./access/Ownable.sol";

contract Deck is ERC1155Holder, Ownable {

    using SafeMath for uint256;

    string public name;
    bool public active;
    uint64 public price;
    uint8 public cardsPerDeck;
    address public payableToken;
    address public erc1155Contract;

    constructor(string memory _name, uint64 _price, uint8 _cardsPerDeck, address _payableToken, address _erc1155Contract) {
        name = _name;
        active = false;
        price = _price;
        cardsPerDeck = _cardsPerDeck;
        payableToken = _payableToken;
        erc1155Contract = _erc1155Contract;
    }

    event DeckActivated();

    event DeckOpened(address indexed operator, uint[] cardsReceived);

//    function createDeck(string memory _name, address _deckAddress, uint64 _price, uint8 _cardsPerDeck) external onlyOwner returns (uint) {
//        require(addressArrayContains(_deckAddresses, _deckAddress), "The deck address is unknown");
//        _checkAvailabilityOfDeckAddress(_deckAddress);
//        decks.push(Deck(_name, false, true, _deckAddress, _price, _cardsPerDeck));
//        return decks.length - 1;
//    }

    function activateDeck() external onlyOwner {
        uint totalBalance = IERC1155(erc1155Contract).totalBalanceOf(address(this));
        require(totalBalance % cardsPerDeck == 0 && totalBalance != 0, "The number of cards in the deck is incorrect");

        active = true;

        DeckActivated();
    }

    function openDeck() external {
        require(active, 'This deck contract is not active');
        require(IERC20(payableToken).transferFrom(_msgSender(), address(this), price), "Impossible to transfer your ERC20 tokens");

        uint256 totalBalance = IERC1155(erc1155Contract).totalBalanceOf(address(this));
        if(totalBalance == cardsPerDeck)active = false;

        uint256[] memory rands = new uint[](cardsPerDeck);
        uint256[] memory choosedCards = new uint[](cardsPerDeck);
        uint256[] memory deckAmounts = IERC1155(erc1155Contract).allBalancesOf(address(this));


        for (uint i = 0 ; i < cardsPerDeck ; i++) {
            // Unsafe random number generator, TODO going to use Chainlink later
            rands[i] = uint256(keccak256(abi.encodePacked(block.timestamp, i))) % (totalBalance - i);
            choosedCards[i] = 0;
        }


        for (uint i = 0 ; i < deckAmounts.length ; i++) {
            if (deckAmounts[i] != 0) {
                for (uint j = 0 ; j < cardsPerDeck ; j++) {
                    if (choosedCards[j] == 0 && deckAmounts[i] != 0) {
                        rands[j] = rands[j].trySub(deckAmounts[i]);

                        if(rands[j] == 0){
                            choosedCards[j] = i + 1;
                            deckAmounts[i] = deckAmounts[i].trySub(1);
                        }
                    }
                }
            }
        }

        for (uint i = 0 ; i < cardsPerDeck ; i++) {
            rands[i] = 1;
        }

        IERC1155(erc1155Contract).safeBatchTransferFrom(address(this), msg.sender, choosedCards, rands, "");

        emit DeckOpened(msg.sender, choosedCards);
    }

    function cashout(address account) external onlyOwner {
        uint256 balance = IERC20(payableToken).balanceOf(address(this));
        IERC20(payableToken).transfer(account, balance);
    }

}
