pragma solidity ^0.8.0;

import "./token/ERC1155/IERC1155.sol";
import "./access/Ownable.sol";
import "./utils/math/SafeMath.sol";
import "./token/ERC1155/utils/ERC1155Holder.sol";

contract ManageCards is Ownable, ERC1155Holder {

    using SafeMath for uint256;

    event CardsMerged(address operator, uint childCard);

    uint[] public childCards;
    address public erc1155Address;

    mapping (uint => uint[]) private _childToParents;

    constructor(address _erc1155Address) {
        erc1155Address = _erc1155Address;
    }

    function setMergeableCard(uint _childCard, uint[] memory _parentCards) external onlyOwner {
        require(_childToParents[_childCard].length == 0, "This card is already set as a childCard");
        _childToParents[_childCard] = _parentCards;
        childCards.push(_childCard);
    }

    function getMergeableCards() external view returns(uint[] memory, uint[][] memory) {
        uint[][] memory _parentCards = new uint[][](childCards.length);
        for (uint i = 0 ; i < childCards.length ; i++) {
            _parentCards[i] = (_childToParents[childCards[i]]);
        }
        return (childCards, _parentCards);
    }

    function mergeCards(address account, uint wantedCardId) external {
        require(_msgSender() == account, "The account is different from the msgSender");

        uint[] memory parentCards = _childToParents[wantedCardId];

        require(parentCards.length > 0, "This card is not available by merging");

        for (uint i = 0 ; i < parentCards.length ; i++) {
            require(IERC1155(erc1155Address).balanceOf(account, parentCards[i]) > 0, "You don't have the necessary cards to merge");
        }
        for (uint i = 0 ; i < parentCards.length ; i++) {
            IERC1155(erc1155Address).burn(account, parentCards[i], 1);
        }

        IERC1155(erc1155Address).mint(account, wantedCardId, 1, '');

        emit CardsMerged(account, wantedCardId);
    }

}
