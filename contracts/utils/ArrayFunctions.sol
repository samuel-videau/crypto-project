pragma solidity ^0.8.0;

contract ArrayFunctions {

    function addressArrayContains(address[] memory array, address element) internal pure returns (bool) {

        for (uint i = 0 ; i < array.length ; i++) {
            if (element == array[i]) return true;
        }

        return false;
    }


}
