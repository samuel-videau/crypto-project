pragma solidity ^0.8.0;

import "./token/ERC20/IERC20.sol";
import "./utils/math/SafeMath.sol";
import "./token/ERC1155/utils/ERC1155Holder.sol";
import "./token/ERC1155/IERC1155.sol";
import "./access/Ownable.sol";

contract Market is ERC1155Holder, Ownable {

    using SafeMath for uint256;

    struct Sale {
        uint price;
        uint16 tokenId;
        address owner;
        bool active;
        bool sold;
    }

    uint public fee;
    address public payableToken;
    address public erc1155Contract;

    Sale[] public sales;

    mapping(uint256 => uint256[]) private _tokenToSales;
    mapping(address => uint256[]) private _ownerToSales;
    mapping(address => uint) private _ownerToClaimable;

    event NewSale(uint saleId, uint indexed tokenId, address indexed owner, uint price);
    event SaleCancelled(uint indexed saleId, uint indexed tokenId, address indexed owner);
    event ItemSold(uint indexed saleId, uint indexed tokenId, address indexed owner, uint price);

    constructor(uint _fee, address _payableToken, address _erc1155Contract) {
        fee = _fee;
        payableToken = _payableToken;
        erc1155Contract = _erc1155Contract;
    }

    function sellToken(uint _tokenId, uint _price) public {
        require(IERC1155(erc1155Contract).isApprovedForAll(_msgSender(), address(this)), "You didn't approved the contract");
        require(IERC1155(erc1155Contract).balanceOf(_msgSender(), _tokenId) > 0, "You don't have an insufficient balance");
        IERC1155(erc1155Contract).safeTransferFrom(_msgSender(), address(this), _tokenId, 1, '');

        sales.push(Sale(_price, uint16(_tokenId), _msgSender(), true, false));
        uint saleId = sales.length;
        _tokenToSales[_tokenId].push(saleId);
        _ownerToSales[_msgSender()].push(saleId);

        emit NewSale(saleId, _tokenId, _msgSender(), _price);
    }

    function cancelSale(uint _saleId) public {
        address owner = sales[_saleId - 1].owner;
        uint tokenId = sales[_saleId - 1].tokenId;
        require(owner == _msgSender(), "You don't have the permission to cancel this sale");

        IERC1155(erc1155Contract).safeTransferFrom(address(this), owner, tokenId, 1, '');

        sales[_saleId - 1].active = false;

        emit SaleCancelled(_saleId, tokenId, owner);
    }

    function buyToken(uint _saleId) public {
        Sale storage sale = sales[_saleId - 1];
        require (sale.active, "Sale is inactive");
        require(IERC20(payableToken).transferFrom(_msgSender(), address(this), sale.price));
        IERC1155(erc1155Contract).safeTransferFrom(address(this), _msgSender(), sale.tokenId, 1, '');

        uint256 priceToTokenOwner = uint256(sale.price * (100 - fee)) / 100;

        _ownerToClaimable[owner()] = sale.price.sub(priceToTokenOwner);
        _ownerToClaimable[sale.owner] = priceToTokenOwner;
        sale.sold = true;
        sale.active = false;

        emit ItemSold(_saleId, sale.tokenId, _msgSender(), sale.price);
    }
    
    function salesOfToken(uint tokenId) public view returns (uint256[] memory){
        return _tokenToSales[tokenId];
    }

    function salesOfAccount(address account) public view returns (uint256[] memory){
        return _ownerToSales[account];
    }

    function cashout(address account) external onlyOwner {
        uint256 balance = IERC20(payableToken).balanceOf(address(this));
        IERC20(payableToken).transfer(account, balance);
    }

    function claimableTokens(address account) public view returns (uint) {
        return _ownerToClaimable[account];
    }

    function claimTokens() external {
        require(_ownerToClaimable[_msgSender()] > 0, "You don't have any token to claim");
        IERC20(payableToken).transfer(_msgSender(), _ownerToClaimable[_msgSender()]);
    }
}
