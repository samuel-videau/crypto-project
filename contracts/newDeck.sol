pragma solidity ^0.8.0;

import "./token/ERC20/IERC20.sol";
import "./utils/math/SafeMath.sol";
import "./token/ERC1155/IERC1155.sol";
import "./access/Ownable.sol";
import "./utils/chainlink/dev/VRFConsumerBase.sol";

contract Decks is Ownable, VRFConsumerBase  {

    using SafeMath for uint256;

    address public erc1155Contract;
    address public payableToken;


    struct Deck {
        string name;
        uint price;
        uint32 totalDecks;
        uint32 openedDecks;
        uint16[] cardIds;
        uint16[] cardChances;
        uint8 cardsPerDeck;
    }

    Deck[] private _decks;

    mapping(bytes32 => address) private _requestIdToAddress;

    bytes32 internal keyHash;
    uint256 internal fee;

    constructor(address _erc1155Contract, address _payableToken) VRFConsumerBase(0x8C7382F9D8f56b33781fE506E897a4F1e2d17255, 0x326C977E6efc84E512bB9C30f76E30c160eD06FB) {
        erc1155Contract = _erc1155Contract;
        payableToken = _payableToken;

        keyHash = 0x6e75b569a01ef56d18cab6a8e71e6600d6ce853834d4a5748b720d06f878b3a4;
        fee = 0.0001 * 10 ** 18;
    }

    event DeckOpened(address indexed operator, uint[] cardsReceived);

    function createDeck(string memory _name, uint _price, uint32 _totalDecks, uint8 _cardsPerDeck, uint16[] memory _cardIds, uint16[] memory _cardChances) external onlyOwner returns (uint) {
        require(_cardIds.length == _cardChances.length, "cards and chances arrays should have same length");

        _decks.push(Deck(_name, _price, _totalDecks, 0, _cardIds, _cardChances, _cardsPerDeck));
        return _decks.length;
    }

    function openDeck(uint id, uint amount) external {
        Deck storage deck = _decks[id - 1];
        require(deck.totalDecks >= deck.openedDecks + amount, "Not enough boosters left in this deck");
        require(IERC20(payableToken).transferFrom(_msgSender(), address(this), deck.price * amount), "Impossible to transfer your ERC20 tokens");

        bytes32 _requestId = getRandomNumber(uint256(keccak256(abi.encodePacked(block.timestamp))));

        _requestIdToAddress[_requestId] = _msgSender();
    }

    function cashout(address account) external onlyOwner {
        uint256 balance = IERC20(payableToken).balanceOf(address(this));
        IERC20(payableToken).transfer(account, balance);
    }

    function getRandomNumber(uint256 userProvidedSeed) public returns (bytes32 requestId) {
        require(LINK.balanceOf(address(this)) >= fee, "Not enough LINK - fill contract with faucet");
        return requestRandomness(keyHash, fee, userProvidedSeed);
    }

    function fulfillRandomness(bytes32 requestId, uint256 randomness) internal override {

    }

    function expand(uint256 randomValue, uint256 n) public pure returns (uint256[] memory expandedValues) {
        expandedValues = new uint256[](n);
        for (uint256 i = 0; i < n; i++) {
            expandedValues[i] = uint256(keccak256(abi.encode(randomValue, i)));
        }
        return expandedValues;
    }

}
